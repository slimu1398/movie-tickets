create: build start compile cc dmm ## Create application from scratch
build: ## Build local images
	docker-compose pull
	docker-compose build
start: ## Start application
	docker-compose up -d
stop: ## Stop application
	docker-compose stop
compile: ## Prepare application codebase
	docker-compose exec movie-tickets-php composer install --prefer-dist
dmm: ## Execute doctrine migrations
	docker-compose exec movie-tickets-php bash -c "bin/console --no-interaction d:m:m"
shell: ## Login to main container with shell
	docker-compose exec movie-tickets-php bash
sa: static-analysis ## Execute static analysis
static-analysis: ## Execute static analysis
	docker-compose exec movie-tickets-php composer sa
phpunit: ## Execute PHPUnit tests
	docker-compose exec movie-tickets-php composer phpunit
cc: ## Clear cache
	docker-compose exec movie-tickets-php bash -c "bin/console cache:clear"