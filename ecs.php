<?php

ini_set('memory_limit', '3G');

use PhpCsFixer\Fixer\ArrayNotation\ArraySyntaxFixer;
use PhpCsFixer\Fixer\ClassNotation\ClassDefinitionFixer;
use PhpCsFixer\Fixer\ClassUsage\DateTimeImmutableFixer;
use PhpCsFixer\Fixer\DoctrineAnnotation\DoctrineAnnotationArrayAssignmentFixer;
use PhpCsFixer\Fixer\DoctrineAnnotation\DoctrineAnnotationSpacesFixer;
use PhpCsFixerCustomFixers\Fixer\CommentSurroundedBySpacesFixer;
use PhpCsFixerCustomFixers\Fixer\DataProviderReturnTypeFixer;
use PhpCsFixerCustomFixers\Fixer\IssetToArrayKeyExistsFixer;
use PhpCsFixerCustomFixers\Fixer\MultilineCommentOpeningClosingAloneFixer;
use PhpCsFixerCustomFixers\Fixer\MultilinePromotedPropertiesFixer;
use PhpCsFixerCustomFixers\Fixer\NoDoctrineMigrationsGeneratedCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoDuplicatedArrayKeyFixer;
use PhpCsFixerCustomFixers\Fixer\NoDuplicatedImportsFixer;
use PhpCsFixerCustomFixers\Fixer\NoNullableBooleanTypeFixer;
use PhpCsFixerCustomFixers\Fixer\NoPhpStormGeneratedCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoTrailingCommaInSinglelineFixer;
use PhpCsFixerCustomFixers\Fixer\NoUselessDoctrineRepositoryCommentFixer;
use PhpCsFixerCustomFixers\Fixer\NoUselessParenthesisFixer;
use PhpCsFixerCustomFixers\Fixer\NoUselessStrlenFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocArrayStyleFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocParamOrderFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocParamTypeFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocSelfAccessorFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocSingleLineVarFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocTypesCommaSpacesFixer;
use PhpCsFixerCustomFixers\Fixer\PhpdocTypesTrimFixer;
use PhpCsFixerCustomFixers\Fixer\PhpUnitAssertArgumentsOrderFixer;
use PhpCsFixerCustomFixers\Fixer\PhpUnitDedicatedAssertFixer;
use PhpCsFixerCustomFixers\Fixer\PromotedConstructorPropertyFixer;
use PhpCsFixerCustomFixers\Fixer\SingleSpaceAfterStatementFixer;
use PhpCsFixerCustomFixers\Fixer\SingleSpaceBeforeStatementFixer;
use PhpCsFixerCustomFixers\Fixer\StringableInterfaceFixer;
use Symplify\EasyCodingStandard\Config\ECSConfig;
use Symplify\EasyCodingStandard\ValueObject\Set\SetList;

return static function (ECSConfig $config): void {
    $config->parallel();
    $config->import(SetList::PSR_12);
    $config->import(SetList::CLEAN_CODE);
    $config->import(SetList::DOCTRINE_ANNOTATIONS);
    $config->import(SetList::STRICT);

    $config->paths([
        __DIR__ . '/src',
        __DIR__ . '/tests'
    ]);

    $config->fileExtensions(['php']);
    $config->cacheDirectory('cache/ecs');

    if (isset($_ENV['CI'])) {
        $config->parallel(600, 4, 50);
    }

    $config->ruleWithConfiguration(ArraySyntaxFixer::class, ['syntax' => 'short']);
    $config->ruleWithConfiguration(ClassDefinitionFixer::class, [
        'multi_line_extends_each_single_line' => true,
        'space_before_parenthesis' => true,
    ]);
    $config->ruleWithConfiguration(DoctrineAnnotationSpacesFixer::class, [
        'before_array_assignments_colon' => false
    ]);
    $config->ruleWithConfiguration(DoctrineAnnotationArrayAssignmentFixer::class, ['operator' => ':']);

    $config->rules([
        CommentSurroundedBySpacesFixer::class,
        DataProviderReturnTypeFixer::class,
        IssetToArrayKeyExistsFixer::class,
        MultilinePromotedPropertiesFixer::class,
        MultilineCommentOpeningClosingAloneFixer::class,
        NoDoctrineMigrationsGeneratedCommentFixer::class,
        NoDuplicatedArrayKeyFixer::class,
        NoDuplicatedImportsFixer::class,
        NoNullableBooleanTypeFixer::class,
        NoPhpStormGeneratedCommentFixer::class,
        NoTrailingCommaInSinglelineFixer::class,
        NoUselessDoctrineRepositoryCommentFixer::class,
        NoUselessParenthesisFixer::class,
        NoUselessStrlenFixer::class,
        PhpUnitAssertArgumentsOrderFixer::class,
        PhpUnitDedicatedAssertFixer::class,
        PhpdocArrayStyleFixer::class,
        PhpdocParamOrderFixer::class,
        PhpdocParamTypeFixer::class,
        PhpdocSelfAccessorFixer::class,
        PhpdocSingleLineVarFixer::class,
        PhpdocTypesCommaSpacesFixer::class,
        PhpdocTypesTrimFixer::class,
        PromotedConstructorPropertyFixer::class,
        SingleSpaceAfterStatementFixer::class,
        SingleSpaceBeforeStatementFixer::class,
        StringableInterfaceFixer::class,
        DateTimeImmutableFixer::class,
    ]);
};
