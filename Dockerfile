FROM php:8.2-fpm-buster as php-base

WORKDIR /build

RUN apt-get update && apt-get install -y \
    gnupg \
    g++ \
    procps \
    openssl \
    git \
    unzip \
    zlib1g-dev \
    libzip-dev \
    libfreetype6-dev \
    libpng-dev \
    libjpeg-dev \
    libicu-dev  \
    libonig-dev \
    libxslt1-dev \
    acl \
    && pecl install \
    xdebug

RUN docker-php-ext-configure gd --with-jpeg --with-freetype

RUN docker-php-ext-install \
    pdo pdo_mysql mysqli zip xsl gd intl opcache exif mbstring bcmath

RUN docker-php-ext-enable xdebug

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

FROM php-base as php-build
COPY composer.json composer.lock .env ./
COPY bin/console ./bin/console
RUN composer install --no-suggest --no-cache --no-scripts
COPY . .

FROM php-base as php-runtime

WORKDIR /var/www/app

COPY --from=php-build /build .