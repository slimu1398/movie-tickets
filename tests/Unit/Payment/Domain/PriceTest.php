<?php

declare(strict_types=1);

namespace App\Tests\Unit\Payment\Domain;

use App\Payment\Domain\Price;
use PHPUnit\Framework\TestCase;
use UnexpectedValueException;

class PriceTest extends TestCase
{
    public function testShouldCreatePriceValueObject(): void
    {
        $price = new Price(0.01);

        self::assertSame(0.01, $price->price);
    }

    public function testShouldThrowExceptionWhenCreatingPriceValueObject(): void
    {
        $this->expectException(UnexpectedValueException::class);

        new Price(0);
    }
}
