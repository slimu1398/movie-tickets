<?php

declare(strict_types=1);

namespace App\Tests\Unit\Payment\Domain;

use App\Payment\Domain\Payment;
use App\Payment\Domain\PaymentStatus;
use App\Payment\Domain\Price;
use PHPUnit\Framework\TestCase;

class PaymentTest extends TestCase
{
    public function testShouldCreatePayment(): void
    {
        $payment = Payment::create(new Price(22));

        self::assertNull($payment->getId()->getId());
        self::assertSame(22.0, $payment->getPrice()->price);
        self::assertSame(PaymentStatus::PENDING, $payment->getStatus());
    }

    public function testShouldCompletePayment(): void
    {
        $payment = Payment::create(new Price(9));
        $payment->complete();

        self::assertSame(PaymentStatus::COMPLETED, $payment->getStatus());
    }
}
