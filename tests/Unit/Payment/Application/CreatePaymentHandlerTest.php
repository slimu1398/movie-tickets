<?php

declare(strict_types=1);

namespace App\Tests\Unit\Payment\Application;

use App\Payment\Application\Command\CreatePaymentCommand;
use App\Payment\Application\CreatePaymentHandler;
use App\Payment\Domain\PaymentRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CreatePaymentHandlerTest extends TestCase
{
    private PaymentRepository&MockObject $paymentRepositoryMock;

    private CreatePaymentHandler $sut;

    protected function setUp(): void
    {
        $this->paymentRepositoryMock = $this->createMock(PaymentRepository::class);

        $this->sut = new CreatePaymentHandler($this->paymentRepositoryMock);
    }

    public function testShouldCreatePayment(): void
    {
        $command = new CreatePaymentCommand(5.79);

        $this->paymentRepositoryMock
            ->expects(self::once())
            ->method('persist');

        $this->sut->__invoke($command);
    }
}
