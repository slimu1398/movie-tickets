<?php

declare(strict_types=1);

namespace App\Tests\Unit\Payment\Application;

use App\Payment\Application\Command\CompletePaymentCommand;
use App\Payment\Application\CompletePaymentHandler;
use App\Payment\Application\Exception\PaymentNotFoundException;
use App\Payment\Domain\Exception\PaymentNotFoundException as PaymentNotFoundDomainException;
use App\Payment\Domain\Payment;
use App\Payment\Domain\PaymentRepository;
use App\Payment\Domain\Price;
use App\Shared\Id;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CompletePaymentHandlerTest extends TestCase
{
    private PaymentRepository&MockObject $paymentRepositoryMock;
    private EventDispatcherInterface&MockObject $dispatcherMock;

    private CompletePaymentHandler $sut;

    protected function setUp(): void
    {
        $this->paymentRepositoryMock = $this->createMock(PaymentRepository::class);
        $this->dispatcherMock = $this->createMock(EventDispatcherInterface::class);

        $this->sut = new CompletePaymentHandler($this->paymentRepositoryMock, $this->dispatcherMock);
    }

    public function testShouldCompletePayment(): void
    {
        $command = new CompletePaymentCommand(new Id(1));

        $this->paymentRepositoryMock
            ->expects(self::once())
            ->method('getById')
            ->willReturn(Payment::create(new Price(99)));

        $this->paymentRepositoryMock
            ->expects(self::once())
            ->method('persist');

        $this->dispatcherMock
            ->expects(self::once())
            ->method('dispatch');

        $this->sut->__invoke($command);
    }

    public function testShouldThrowExceptionWhenPaymentNotFound(): void
    {
        $this->expectException(PaymentNotFoundException::class);

        $command = new CompletePaymentCommand(new Id(1));

        $this->paymentRepositoryMock
            ->expects(self::once())
            ->method('getById')
            ->willThrowException(PaymentNotFoundDomainException::create());

        $this->paymentRepositoryMock
            ->expects(self::never())
            ->method('persist');

        $this->dispatcherMock
            ->expects(self::never())
            ->method('dispatch');

        $this->sut->__invoke($command);
    }
}
