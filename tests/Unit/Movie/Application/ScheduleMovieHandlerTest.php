<?php

declare(strict_types=1);

namespace App\Tests\Unit\Movie\Application;

use App\Movie\Application\Command\ScheduleMovieCommand;
use App\Movie\Application\Exception\MovieNotFoundException;
use App\Movie\Application\Exception\MovieScheduleDateOccupiedException;
use App\Movie\Application\ScheduleMovieHandler;
use App\Movie\Domain\Exception\CannotScheduleMovieWithGivenDateException;
use App\Movie\Domain\Exception\MovieNotFoundException as MovieNotFoundDomainException;
use App\Movie\Domain\Movie;
use App\Movie\Domain\MovieId;
use App\Movie\Domain\MovieRepository;
use App\Movie\Domain\MovieScheduleAvailabilityService;
use App\Movie\Domain\MovieScheduleRepository;
use App\Shared\Id;
use Carbon\CarbonImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ScheduleMovieHandlerTest extends TestCase
{
    private MovieRepository&MockObject $movieRepositoryMock;
    private MovieScheduleRepository&MockObject $movieScheduleRepositoryMock;
    private MovieScheduleAvailabilityService&MockObject $movieScheduleAvailabilityServiceMock;

    private ScheduleMovieHandler $sut;

    protected function setUp(): void
    {
        $this->movieRepositoryMock = $this->createMock(MovieRepository::class);
        $this->movieScheduleRepositoryMock = $this->createMock(MovieScheduleRepository::class);
        $this->movieScheduleAvailabilityServiceMock = $this->createMock(MovieScheduleAvailabilityService::class);

        $this->sut = new ScheduleMovieHandler(
            $this->movieRepositoryMock,
            $this->movieScheduleRepositoryMock,
            $this->movieScheduleAvailabilityServiceMock
        );
    }

    public function testShouldScheduleMovie(): void
    {
        $movieId = new Id(1);
        $screenId = new Id(5);
        $movieStartDate = new CarbonImmutable('2023-09-11 21:30:00');
        $command = new ScheduleMovieCommand($movieId, $screenId, $movieStartDate);
        $movie = new Movie(MovieId::fromId($movieId), 'name', 90);

        $this->movieRepositoryMock
            ->expects(self::once())
            ->method('getById')
            ->willReturn($movie);

        $this->movieScheduleAvailabilityServiceMock
            ->expects(self::once())
            ->method('checkMovieScheduleAvailability');

        $this->movieScheduleRepositoryMock
            ->expects(self::once())
            ->method('persist');

        $this->sut->__invoke($command);
    }

    public function testShouldThrowMovieNotFoundException(): void
    {
        $this->expectException(MovieNotFoundException::class);

        $movieId = new Id(1);
        $screenId = new Id(5);
        $movieStartDate = new CarbonImmutable('2023-09-11 21:30:00');
        $command = new ScheduleMovieCommand($movieId, $screenId, $movieStartDate);

        $this->movieRepositoryMock
            ->expects(self::once())
            ->method('getById')
            ->willThrowException(MovieNotFoundDomainException::create());

        $this->movieScheduleAvailabilityServiceMock
            ->expects(self::never())
            ->method('checkMovieScheduleAvailability');

        $this->movieScheduleRepositoryMock
            ->expects(self::never())
            ->method('persist');

        $this->sut->__invoke($command);
    }

    public function testShouldThrowMovieScheduleDateOccupiedException(): void
    {
        $this->expectException(MovieScheduleDateOccupiedException::class);

        $movieId = new Id(1);
        $screenId = new Id(5);
        $movieStartDate = new CarbonImmutable('2023-09-11 21:30:00');
        $command = new ScheduleMovieCommand($movieId, $screenId, $movieStartDate);
        $movie = new Movie(MovieId::fromId($movieId), 'name', 90);

        $this->movieRepositoryMock
            ->expects(self::once())
            ->method('getById')
            ->willReturn($movie);

        $this->movieScheduleAvailabilityServiceMock
            ->expects(self::once())
            ->method('checkMovieScheduleAvailability')
            ->willThrowException(CannotScheduleMovieWithGivenDateException::create());

        $this->movieScheduleRepositoryMock
            ->expects(self::never())
            ->method('persist');

        $this->sut->__invoke($command);
    }
}
