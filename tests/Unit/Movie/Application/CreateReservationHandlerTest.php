<?php

declare(strict_types=1);

namespace App\Tests\Unit\Movie\Application;

use App\Movie\Application\Command\CreateReservationCommand;
use App\Movie\Application\CreateReservationHandler;
use App\Movie\Application\Exception\MovieScheduleNotFoundException;
use App\Movie\Domain\Exception\MovieScheduleNotFoundException as MovieScheduleNotFoundDomainException;
use App\Movie\Domain\Movie;
use App\Movie\Domain\MovieId;
use App\Movie\Domain\MovieScheduleAvailabilityService;
use App\Movie\Domain\MovieScheduleRepository;
use App\Movie\Domain\PaymentId;
use App\Movie\Domain\PaymentService;
use App\Movie\Domain\ScreenId;
use App\Shared\Id;
use Carbon\CarbonImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class CreateReservationHandlerTest extends TestCase
{
    private MovieScheduleRepository&MockObject $movieScheduleRepositoryMock;
    private MovieScheduleAvailabilityService&MockObject $movieScheduleAvailabilityServiceMock;
    private PaymentService&MockObject $paymentServiceMock;

    private CreateReservationHandler $sut;

    protected function setUp(): void
    {
        $this->movieScheduleRepositoryMock = $this->createMock(MovieScheduleRepository::class);
        $this->movieScheduleAvailabilityServiceMock = $this->createMock(MovieScheduleAvailabilityService::class);
        $this->paymentServiceMock = $this->createMock(PaymentService::class);

        $this->sut = new CreateReservationHandler(
            $this->movieScheduleRepositoryMock,
            $this->paymentServiceMock
        );
    }

    public function testShouldCreateReservation(): void
    {
        $movieScheduleId = new Id(1);
        $screenId = new ScreenId(1);
        $seats = [1, 2];
        $command = new CreateReservationCommand($movieScheduleId, $seats);
        $movie = new Movie(new MovieId(1), 'name', 90);
        $movieSchedule = $movie->schedule(
            $screenId,
            new CarbonImmutable('2023-08-19 21:00:00'),
            $this->movieScheduleAvailabilityServiceMock
        );

        $this->movieScheduleRepositoryMock
            ->expects(self::once())
            ->method('getById')
            ->willReturn($movieSchedule);

        $this->paymentServiceMock
            ->expects(self::once())
            ->method('createPayment')
            ->willReturn(new PaymentId(1));

        $this->movieScheduleRepositoryMock
            ->expects(self::once())
            ->method('persist');

        $this->sut->__invoke($command);
    }

    public function testShouldThrowNamedExceptionWhenCreatingReservation(): void
    {
        $this->expectException(MovieScheduleNotFoundException::class);

        $movieScheduleId = new Id(1);
        $seats = [1, 2];
        $command = new CreateReservationCommand($movieScheduleId, $seats);

        $this->movieScheduleRepositoryMock
            ->expects(self::once())
            ->method('getById')
            ->willThrowException(MovieScheduleNotFoundDomainException::create());

        $this->paymentServiceMock
            ->expects(self::never())
            ->method('createPayment');

        $this->movieScheduleRepositoryMock
            ->expects(self::never())
            ->method('persist');

        $this->sut->__invoke($command);
    }
}
