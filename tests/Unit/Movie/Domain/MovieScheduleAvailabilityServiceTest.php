<?php

declare(strict_types=1);

namespace App\Tests\Unit\Movie\Domain;

use App\Movie\Domain\Exception\CannotScheduleMovieWithGivenDateException;
use App\Movie\Domain\Exception\MovieScheduleNotFoundException;
use App\Movie\Domain\MovieScheduleAvailabilityService;
use App\Movie\Domain\MovieScheduleRepository;
use App\Movie\Domain\MovieScheduleRepositoryQuery;
use App\Movie\Domain\ScreenId;
use Carbon\CarbonImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MovieScheduleAvailabilityServiceTest extends TestCase
{
    private MovieScheduleRepository&MockObject $movieScheduleRepositoryMock;

    private MovieScheduleAvailabilityService $sut;

    protected function setUp(): void
    {
        $this->movieScheduleRepositoryMock = $this->createMock(MovieScheduleRepository::class);

        $this->sut = new MovieScheduleAvailabilityService($this->movieScheduleRepositoryMock);
    }

    public function testShouldCheckThatMovieScheduleIsAvailable(): void
    {
        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-07-15 17:30:00');
        $movieEndDate = new CarbonImmutable('2023-07-15 19:30:00');

        $this->movieScheduleRepositoryMock
            ->expects(self::once())
            ->method('getByQuery')
            ->with(
                new MovieScheduleRepositoryQuery(
                    $screenId,
                    new CarbonImmutable('2023-07-15 20:00:00'),
                    new CarbonImmutable('2023-07-15 17:00:00')
                )
            )
            ->willThrowException(MovieScheduleNotFoundException::create());

        $this->sut->checkMovieScheduleAvailability(
            $screenId,
            $movieStartDate,
            $movieEndDate
        );
    }

    public function testShouldCheckThatMovieScheduleIsUnavailable(): void
    {
        $this->expectException(CannotScheduleMovieWithGivenDateException::class);

        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-07-15 17:30:00');
        $movieEndDate = new CarbonImmutable('2023-07-15 19:30:00');

        $this->movieScheduleRepositoryMock
            ->expects(self::once())
            ->method('getByQuery')
            ->with(
                new MovieScheduleRepositoryQuery(
                    $screenId,
                    new CarbonImmutable('2023-07-15 20:00:00'),
                    new CarbonImmutable('2023-07-15 17:00:00')
                )
            );

        $this->sut->checkMovieScheduleAvailability(
            $screenId,
            $movieStartDate,
            $movieEndDate
        );
    }
}
