<?php

declare(strict_types=1);

namespace App\Tests\Unit\Movie\Domain;

use App\Movie\Domain\Exception\CannotScheduleMovieWithGivenDateException;
use App\Movie\Domain\Movie;
use App\Movie\Domain\MovieId;
use App\Movie\Domain\MovieSchedule;
use App\Movie\Domain\MovieScheduleAvailabilityService;
use App\Movie\Domain\ScreenId;
use Carbon\CarbonImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MovieScheduleTest extends TestCase
{
    private MovieScheduleAvailabilityService&MockObject $movieScheduleAvailabilityServiceMock;

    protected function setUp(): void
    {
        $this->movieScheduleAvailabilityServiceMock = $this->createMock(MovieScheduleAvailabilityService::class);
    }

    public function testShouldCreateMovieSchedule(): void
    {
        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-09-20 20:00:00');
        $movieEndDate = new CarbonImmutable('2023-09-20 21:30:00');
        $movie = new Movie(new MovieId(1), 'name', 90);

        $this->movieScheduleAvailabilityServiceMock
            ->expects(self::once())
            ->method('checkMovieScheduleAvailability')
            ->with($screenId, $movieStartDate, $movieEndDate);

        $movieSchedule = MovieSchedule::create(
            $movie,
            $screenId,
            $movieStartDate,
            $this->movieScheduleAvailabilityServiceMock
        );

        self::assertNull($movieSchedule->getId()->getId());
        self::assertSame((string)$movieEndDate, (string)$movieSchedule->movieEndDate);
        self::assertCount(0, $movieSchedule->getMovieReservations());
    }

    public function testMovieScheduleAvailabilityServiceShouldThrowException(): void
    {
        $this->expectException(CannotScheduleMovieWithGivenDateException::class);

        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-09-20 20:00:00');
        $movie = new Movie(new MovieId(1), 'name', 90);

        $this->movieScheduleAvailabilityServiceMock
            ->expects(self::once())
            ->method('checkMovieScheduleAvailability')
            ->willThrowException(CannotScheduleMovieWithGivenDateException::create());

        MovieSchedule::create(
            $movie,
            $screenId,
            $movieStartDate,
            $this->movieScheduleAvailabilityServiceMock
        );
    }

    public function testShouldCreateTwoReservations(): void
    {
        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-09-20 20:00:00');
        $movieEndDate = new CarbonImmutable('2023-09-20 21:30:00');
        $movie = new Movie(new MovieId(1), 'name', 90);

        $this->movieScheduleAvailabilityServiceMock
            ->expects(self::once())
            ->method('checkMovieScheduleAvailability')
            ->with($screenId, $movieStartDate, $movieEndDate);

        $movieSchedule = MovieSchedule::create(
            $movie,
            $screenId,
            $movieStartDate,
            $this->movieScheduleAvailabilityServiceMock
        );

        $movieSchedule->createReservation([30, 31]);
        $movieSchedule->createReservation([9]);

        self::assertCount(2, $movieSchedule->getMovieReservations());
    }

    /**
     * @param array<int, int> $seats
     * @dataProvider seatsOccupiedDataProvider
     */
    public function testShouldVerifyIfSeatsAreOccupied(array $seats, bool $areSeatsOccupied): void
    {
        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-09-20 20:00:00');
        $movieEndDate = new CarbonImmutable('2023-09-20 21:30:00');
        $movie = new Movie(new MovieId(1), 'name', 90);

        $this->movieScheduleAvailabilityServiceMock
            ->expects(self::once())
            ->method('checkMovieScheduleAvailability')
            ->with($screenId, $movieStartDate, $movieEndDate);

        $movieSchedule = MovieSchedule::create(
            $movie,
            $screenId,
            $movieStartDate,
            $this->movieScheduleAvailabilityServiceMock
        );

        $movieSchedule->createReservation([30, 31]);
        $movieSchedule->createReservation([9]);

        self::assertSame($areSeatsOccupied, $movieSchedule->areSeatsOccupied($seats));
    }

    public function seatsOccupiedDataProvider(): iterable
    {
        return [
            [[30, 40, 50], true],
            [[9], true],
            [[1], false],
            [[29, 32, 100], false]
        ];
    }
}
