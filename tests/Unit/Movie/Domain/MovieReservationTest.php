<?php

declare(strict_types=1);

namespace App\Tests\Unit\Movie\Domain;

use App\Movie\Domain\Exception\SeatsOccupiedException;
use App\Movie\Domain\Movie;
use App\Movie\Domain\MovieId;
use App\Movie\Domain\MovieReservation;
use App\Movie\Domain\MovieScheduleAvailabilityService;
use App\Movie\Domain\PaymentId;
use App\Movie\Domain\PaymentService;
use App\Movie\Domain\ScreenId;
use Carbon\CarbonImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class MovieReservationTest extends TestCase
{
    private MovieScheduleAvailabilityService&MockObject $movieScheduleAvailabilityServiceMock;
    private PaymentService&MockObject $paymentServiceMock;

    protected function setUp(): void
    {
        $this->movieScheduleAvailabilityServiceMock = $this->createMock(MovieScheduleAvailabilityService::class);
        $this->paymentServiceMock = $this->createMock(PaymentService::class);
    }

    public function testShouldCreateReservation(): void
    {
        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-09-20 20:00:00');
        $movie = new Movie(new MovieId(1), 'name', 90);

        $movieSchedule = $movie->schedule($screenId, $movieStartDate, $this->movieScheduleAvailabilityServiceMock);

        $movieReservation = MovieReservation::create($movieSchedule, [5, 6, 7, 8]);

        self::assertNull($movieReservation->getId()->getId());
        self::assertNull($movieReservation->getPaymentId()->getId());
        self::assertEquals([5, 6, 7, 8], $movieReservation->seats);
        self::assertSame((string) new CarbonImmutable('2023-09-20 19:30:00'), (string)$movieReservation->getExpirationDate());
    }

    public function testShouldThrowSeatsOccupiedExceptionWhenCreatingReservation(): void
    {
        $this->expectException(SeatsOccupiedException::class);

        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-09-20 20:00:00');
        $movie = new Movie(new MovieId(1), 'name', 90);

        $movieSchedule = $movie->schedule($screenId, $movieStartDate, $this->movieScheduleAvailabilityServiceMock);
        $movieSchedule->createReservation([5]);

        MovieReservation::create($movieSchedule, [5, 6, 7, 8]);
    }

    public function testShouldCalculateReservationPrice(): void
    {
        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-09-20 20:00:00');
        $movie = new Movie(new MovieId(1), 'name', 90);

        $movieSchedule = $movie->schedule($screenId, $movieStartDate, $this->movieScheduleAvailabilityServiceMock);

        $movieReservation = MovieReservation::create($movieSchedule, [5, 6, 7, 8]);

        self::assertSame(71.96, $movieReservation->calculateReservationPrice());
    }

    public function testShouldCreatePayment(): void
    {
        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-09-20 20:00:00');
        $movie = new Movie(new MovieId(1), 'name', 90);

        $movieSchedule = $movie->schedule($screenId, $movieStartDate, $this->movieScheduleAvailabilityServiceMock);

        $movieReservation = MovieReservation::create($movieSchedule, [5, 6, 7, 8]);

        $this->paymentServiceMock
            ->expects(self::once())
            ->method('createPayment')
            ->willReturn(new PaymentId(1));

        $movieReservation->createPayment($this->paymentServiceMock);
    }

    public function testShouldCompleteReservation(): void
    {
        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-09-20 20:00:00');
        $movie = new Movie(new MovieId(1), 'name', 90);

        $movieSchedule = $movie->schedule($screenId, $movieStartDate, $this->movieScheduleAvailabilityServiceMock);

        $movieReservation = MovieReservation::create($movieSchedule, [5, 6, 7, 8]);
        $movieReservation->complete();

        self::assertNull($movieReservation->getExpirationDate());
    }
}
