<?php

declare(strict_types=1);

namespace App\Tests\Unit\Movie\Infrastructure;

use App\Movie\Domain\Movie;
use App\Movie\Domain\MovieId;
use App\Movie\Domain\MovieReservation;
use App\Movie\Domain\MovieScheduleAvailabilityService;
use App\Movie\Domain\ScreenId;
use App\Movie\Infrastructure\MovieReservationDoctrineRepository;
use App\Movie\Infrastructure\PaymentCompletedEventListener;
use App\Payment\Application\Event\PaymentCompletedEvent;
use App\Shared\Id;
use Carbon\CarbonImmutable;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class PaymentCompletedEventListenerTest extends TestCase
{
    private MovieScheduleAvailabilityService&MockObject $movieScheduleAvailabilityServiceMock;
    private MovieReservationDoctrineRepository&MockObject $repositoryMock;

    private PaymentCompletedEventListener $sut;

    protected function setUp(): void
    {
        $this->movieScheduleAvailabilityServiceMock = $this->createMock(MovieScheduleAvailabilityService::class);
        $this->repositoryMock = $this->createMock(MovieReservationDoctrineRepository::class);

        $this->sut = new PaymentCompletedEventListener($this->repositoryMock);
    }

    public function testShouldCompleteMovieReservation(): void
    {
        $event = new PaymentCompletedEvent(new Id(4));
        $screenId = new ScreenId(1);
        $movieStartDate = new CarbonImmutable('2023-09-20 20:00:00');
        $movie = new Movie(new MovieId(1), 'name', 90);

        $movieSchedule = $movie->schedule($screenId, $movieStartDate, $this->movieScheduleAvailabilityServiceMock);

        $movieReservation = MovieReservation::create($movieSchedule, [5, 6, 7, 8]);

        $this->repositoryMock
            ->expects(self::once())
            ->method('findByPaymentId')
            ->willReturn($movieReservation);

        $this->repositoryMock
            ->expects(self::once())
            ->method('persist');

        $this->sut->__invoke($event);

        self::assertNull($movieReservation->getExpirationDate());
    }

    public function testShouldNotCompleteMovieReservationWhenReservationNotFound(): void
    {
        $event = new PaymentCompletedEvent(new Id(4));

        $this->repositoryMock
            ->expects(self::once())
            ->method('findByPaymentId')
            ->willReturn(null);

        $this->repositoryMock
            ->expects(self::never())
            ->method('persist');

        $this->sut->__invoke($event);
    }
}
