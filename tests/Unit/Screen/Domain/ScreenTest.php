<?php

declare(strict_types=1);

namespace App\Tests\Unit\Screen\Domain;

use App\Screen\Domain\Screen;
use App\Screen\Domain\ScreenId;
use App\Screen\Domain\Seat;
use PHPUnit\Framework\TestCase;

class ScreenTest extends TestCase
{
    public function testShouldReturnProperSeatNumbers(): void
    {
        $screen = new Screen(new ScreenId(1), 'name', [new Seat(5), new Seat(77)]);

        self::assertEquals([5, 77], $screen->getSeatNumbers());
    }
}
