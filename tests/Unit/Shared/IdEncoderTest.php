<?php

declare(strict_types=1);

namespace App\Tests\Unit\Shared;

use App\Shared\IdEncoder;
use PHPUnit\Framework\TestCase;

class IdEncoderTest extends TestCase
{
    /**
     * @dataProvider hashIdDataProvider
     */
    public function testShouldEncodeId(string $expectedHash, int $id): void
    {
        $hash = IdEncoder::encode($id);
        self::assertSame($expectedHash, $hash);
    }

    /**
     * @dataProvider hashIdDataProvider
     */
    public function testShouldDecodeId(string $hash, int $expectedId): void
    {
        $id = IdEncoder::decode($hash);
        self::assertSame($expectedId, $id);
    }

    /**
     * @return array<int, array<int, int|string>>
     */
    public function hashIdDataProvider(): iterable
    {
        return [
            ['jVJL7xRO', 1],
            ['Vow13MJO', 50],
            ['2jGOD6wy', 999],
            ['GM92enrG', 7194721]
        ];
    }
}
