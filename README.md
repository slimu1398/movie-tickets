# Movie Tickets

### Running application:
Type `make create` to run application.
If everything is set up, http://localhost:22001/ping should return successful response.
Import `movie-tickets-postman-collection.json` into Postman to set up all requests.

### Encoding IDs:
Get inside php container (`make shell`) and execute command `bin/console app:shared:encode-id <id>` to encode integer ID to hashed ID.

#### TODO:
- Command/Cron for removing unpaid reservations (30 min before movie starts)
- Movie association with MovieSchedule
- When scheduling movie, check if Screen exists
- When creating reservation, check if chosen seat exists for that Screen
- Check if payment is completed when completing
- Object builder / Object Mother pattern for creating objects in tests
- When creating payment, pass unique uuid to command and use it in events, instead of returning id from db