<?php

declare(strict_types=1);

namespace App\Shared;

use Hashids\Hashids;

class IdEncoder
{
    private const SALT = 'iUCslj4dvBLmM5x';
    private const MIN_HASH_LENGTH = 8;

    public static function encode(int $id): string
    {
        $hashids = new Hashids(self::SALT, self::MIN_HASH_LENGTH);

        return $hashids->encode($id);
    }

    public static function decode(string $hash): int
    {
        $hashids = new Hashids(self::SALT, self::MIN_HASH_LENGTH);

        return $hashids->decode($hash)[0];
    }
}
