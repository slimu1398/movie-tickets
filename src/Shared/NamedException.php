<?php

declare(strict_types=1);

namespace App\Shared;

use Exception;

abstract class NamedException extends Exception
{
    abstract public function getName(): string;
}
