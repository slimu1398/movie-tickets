<?php

declare(strict_types=1);

namespace App\Shared;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\HandlerFailedException;

class ApiController extends AbstractController
{
    public function handlerException(HandlerFailedException $e): JsonResponse
    {
        if ($e->getPrevious() instanceof NamedException) {
            return $this->namedException($e->getPrevious());
        }

        return new JsonResponse(
            [
                'name' => 'handler.generalException',
                'message' => $e->getMessage()
            ],
            Response::HTTP_BAD_REQUEST
        );
    }

    private function namedException(NamedException $e): JsonResponse
    {
        return new JsonResponse(
            [
                'name' => $e->getName(),
                'message' => $e->getMessage()
            ],
            Response::HTTP_BAD_REQUEST
        );
    }
}
