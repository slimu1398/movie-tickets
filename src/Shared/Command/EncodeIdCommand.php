<?php

declare(strict_types=1);

namespace App\Shared\Command;

use App\Shared\IdEncoder;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:shared:encode-id', description: 'Used to encode integer ID to hashed ID')]
class EncodeIdCommand extends Command
{
    protected function configure(): void
    {
        $this->addArgument('id', InputArgument::REQUIRED, 'integer ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /* @phpstan-ignore-next-line */
        $id = (int)$input->getArgument('id');

        $output->writeln(IdEncoder::encode($id));

        return Command::SUCCESS;
    }
}
