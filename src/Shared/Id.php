<?php

declare(strict_types=1);

namespace App\Shared;

use UnexpectedValueException;

readonly class Id
{
    final public function __construct(
        private ?int $id
    ) {
    }

    public static function nullInstance(): static
    {
        return new static(null);
    }

    public static function fromId(Id $id): static
    {
        return new static($id->getId());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntValue(): int
    {
        if (null === $this->id) {
            throw new UnexpectedValueException('Id cannot be null');
        }

        return $this->id;
    }

    public function encode(): string
    {
        if (null === $this->id) {
            throw new UnexpectedValueException('Id cannot be null');
        }

        return IdEncoder::encode($this->id);
    }

    public static function fromEncodedId(string $encodedId): static
    {
        return new static(IdEncoder::decode($encodedId));
    }
}
