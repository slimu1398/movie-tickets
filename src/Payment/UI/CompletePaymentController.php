<?php

declare(strict_types=1);

namespace App\Payment\UI;

use App\Payment\Application\Command\CompletePaymentCommand;
use App\Shared\ApiController;
use App\Shared\Id;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class CompletePaymentController extends ApiController
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $messageBus
    ) {
    }

    #[Route('/{paymentId}/complete', name: 'complete', requirements: ['paymentId' => '^[a-zA-Z0-9]+'], methods: ['GET'])]
    public function __invoke(string $paymentId): JsonResponse
    {
        try {
            $this->handle(
                new CompletePaymentCommand(Id::fromEncodedId($paymentId))
            );
        } catch (HandlerFailedException $e) {
            return $this->handlerException($e);
        }

        return new JsonResponse();
    }
}
