<?php

declare(strict_types=1);

namespace App\Payment\Domain;

use App\Payment\Domain\Exception\PaymentNotFoundException;

interface PaymentRepository
{
    public function persist(Payment $payment): void;

    /**
     * @throws PaymentNotFoundException
     */
    public function getById(PaymentId $paymentId): Payment;
}
