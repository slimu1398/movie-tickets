<?php

declare(strict_types=1);

namespace App\Payment\Domain;

use App\Shared\Id;

readonly class PaymentId extends Id
{
}
