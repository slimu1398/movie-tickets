<?php

declare(strict_types=1);

namespace App\Payment\Domain;

use UnexpectedValueException;

readonly class Price
{
    public function __construct(
        public float $price
    ) {
        $this->validate();
    }

    private function validate(): void
    {
        if ($this->price <= 0) {
            throw new UnexpectedValueException('Price must be greater than 0');
        }
    }
}
