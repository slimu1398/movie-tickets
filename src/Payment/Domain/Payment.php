<?php

declare(strict_types=1);

namespace App\Payment\Domain;

class Payment
{
    private ?int $id;
    private readonly float $price;

    private function __construct(
        PaymentId $paymentId,
        Price $price,
        private PaymentStatus $status
    ) {
        $this->id = $paymentId->getId();
        $this->price = $price->price;
    }

    public static function create(Price $price): self
    {
        return new self(PaymentId::nullInstance(), $price, PaymentStatus::PENDING);
    }

    public function getId(): PaymentId
    {
        return new PaymentId($this->id);
    }

    public function getPrice(): Price
    {
        return new Price($this->price);
    }

    public function getStatus(): PaymentStatus
    {
        return $this->status;
    }

    public function complete(): void
    {
        $this->status = PaymentStatus::COMPLETED;
    }
}
