<?php

declare(strict_types=1);

namespace App\Payment\Domain\Exception;

use Exception;

class PaymentNotFoundException extends Exception
{
    public static function create(): self
    {
        return new self('Payment not found');
    }
}
