<?php

declare(strict_types=1);

namespace App\Payment\Application;

use App\Payment\Application\Command\CreatePaymentCommand;
use App\Payment\Domain\Payment;
use App\Payment\Domain\PaymentRepository;
use App\Payment\Domain\Price;
use App\Shared\Id;

readonly class CreatePaymentHandler
{
    public function __construct(
        private PaymentRepository $paymentRepository
    ) {
    }

    public function __invoke(CreatePaymentCommand $command): Id
    {
        $payment = Payment::create(new Price($command->price));

        $this->paymentRepository->persist($payment);

        return Id::fromId($payment->getId());
    }
}
