<?php

declare(strict_types=1);

namespace App\Payment\Application\Exception;

use App\Shared\NamedException;

abstract class PaymentNamedException extends NamedException
{
    protected const PAYMENT_NOT_FOUND_NAME = 'payment.notFound';
    protected const PAYMENT_NOT_FOUND_CODE = 2001;
}
