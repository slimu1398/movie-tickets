<?php

declare(strict_types=1);

namespace App\Payment\Application\Exception;

class PaymentNotFoundException extends PaymentNamedException
{
    public static function create(): self
    {
        return new self('Payment not found', self::PAYMENT_NOT_FOUND_CODE);
    }

    public function getName(): string
    {
        return self::PAYMENT_NOT_FOUND_NAME;
    }
}
