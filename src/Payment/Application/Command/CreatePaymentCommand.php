<?php

declare(strict_types=1);

namespace App\Payment\Application\Command;

readonly class CreatePaymentCommand
{
    public function __construct(
        public float $price
    ) {
    }
}
