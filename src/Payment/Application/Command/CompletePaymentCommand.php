<?php

declare(strict_types=1);

namespace App\Payment\Application\Command;

use App\Shared\Id;

readonly class CompletePaymentCommand
{
    public function __construct(
        public Id $paymentId
    ) {
    }
}
