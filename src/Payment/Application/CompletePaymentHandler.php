<?php

declare(strict_types=1);

namespace App\Payment\Application;

use App\Payment\Application\Command\CompletePaymentCommand;
use App\Payment\Application\Event\PaymentCompletedEvent;
use App\Payment\Application\Exception\PaymentNotFoundException;
use App\Payment\Domain\Exception\PaymentNotFoundException as PaymentNotFoundDomainException;
use App\Payment\Domain\PaymentId;
use App\Payment\Domain\PaymentRepository;
use App\Shared\Id;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class CompletePaymentHandler
{
    public function __construct(
        private PaymentRepository $paymentRepository,
        private EventDispatcherInterface $dispatcher
    ) {
    }

    /**
     * @throws PaymentNotFoundException
     */
    public function __invoke(CompletePaymentCommand $command): void
    {
        try {
            $payment = $this->paymentRepository->getById(PaymentId::fromId($command->paymentId));
            $payment->complete();

            $this->paymentRepository->persist($payment);

            $this->dispatcher->dispatch(
                new PaymentCompletedEvent(Id::fromId($payment->getId()))
            );
        } catch (PaymentNotFoundDomainException) {
            throw PaymentNotFoundException::create();
        }
    }
}
