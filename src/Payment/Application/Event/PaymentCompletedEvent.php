<?php

declare(strict_types=1);

namespace App\Payment\Application\Event;

use App\Shared\Id;
use Symfony\Contracts\EventDispatcher\Event;

class PaymentCompletedEvent extends Event
{
    public function __construct(
        public readonly Id $paymentId
    ) {
    }
}
