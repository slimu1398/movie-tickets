<?php

declare(strict_types=1);

namespace App\Payment\Infrastructure;

use App\Payment\Domain\Exception\PaymentNotFoundException;
use App\Payment\Domain\Payment;
use App\Payment\Domain\PaymentId;
use App\Payment\Domain\PaymentRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Payment>
 */
class PaymentDoctrineRepository extends ServiceEntityRepository implements PaymentRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    public function persist(Payment $payment): void
    {
        $this->getEntityManager()->persist($payment);
        $this->getEntityManager()->flush();
    }

    public function getById(PaymentId $paymentId): Payment
    {
        $payment = $this->find($paymentId->getId());

        if (null === $payment) {
            throw PaymentNotFoundException::create();
        }

        return $payment;
    }
}
