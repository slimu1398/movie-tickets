<?php

declare(strict_types=1);

namespace App\Screen\Infrastructure;

use App\Screen\Domain\Screen;
use App\Screen\Domain\ScreenId;
use App\Screen\Domain\ScreenRepository;
use App\Screen\Domain\Seat;

class ScreenInMemoryRepository implements ScreenRepository
{
    public function getScreens(): array
    {
        return [
            new Screen(new ScreenId(1), 'screen_1', $this->createSeats(500)),
            new Screen(new ScreenId(2), 'screen_2', $this->createSeats(25)),
            new Screen(new ScreenId(3), 'screen_3', $this->createSeats(90))
        ];
    }

    /**
     * @return array<int, Seat>
     */
    private function createSeats(int $seatsAmount): array
    {
        return array_map(fn (int $seatNumber): Seat => new Seat($seatNumber), range(1, $seatsAmount));
    }
}
