<?php

declare(strict_types=1);

namespace App\Screen\Domain;

readonly class Seat
{
    public function __construct(
        public int $number
    ) {
    }
}
