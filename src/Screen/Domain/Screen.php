<?php

declare(strict_types=1);

namespace App\Screen\Domain;

readonly class Screen
{
    /**
     * @param array<int, Seat> $seats
     */
    public function __construct(
        public ScreenId $screenId,
        public string $name,
        public array $seats
    ) {
    }

    /**
     * @return array<int, int>
     */
    public function getSeatNumbers(): array
    {
        return array_map(fn (Seat $seat): int => $seat->number, $this->seats);
    }
}
