<?php

declare(strict_types=1);

namespace App\Screen\Domain;

interface ScreenRepository
{
    /**
     * @return array<int, Screen>
     */
    public function getScreens(): array;
}
