<?php

declare(strict_types=1);

namespace App\Screen\Domain;

use App\Shared\Id;

readonly class ScreenId extends Id
{
}
