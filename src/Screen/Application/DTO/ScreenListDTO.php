<?php

declare(strict_types=1);

namespace App\Screen\Application\DTO;

use App\Screen\Domain\Screen;

readonly class ScreenListDTO
{
    /**
     * @param array<int, ScreenDTO> $screenDTOs
     */
    public function __construct(
        public array $screenDTOs
    ) {
    }

    /**
     * @param array<int, Screen> $screens
     */
    public static function createFromScreens(array $screens): self
    {
        $screenListDTO = [];
        foreach ($screens as $screen) {
            $screenListDTO[] = ScreenDTO::createFromScreen($screen);
        }

        return new self($screenListDTO);
    }
}
