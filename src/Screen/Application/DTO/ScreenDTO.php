<?php

declare(strict_types=1);

namespace App\Screen\Application\DTO;

use App\Screen\Domain\Screen;

readonly class ScreenDTO
{
    /**
     * @param array<int, int> $seats
     */
    public function __construct(
        public int $id,
        public string $name,
        public array $seats
    ) {
    }

    public static function createFromScreen(Screen $screen): self
    {
        return new self(
            $screen->screenId->getIntValue(),
            $screen->name,
            $screen->getSeatNumbers()
        );
    }
}
