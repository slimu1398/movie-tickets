<?php

declare(strict_types=1);

namespace App\Screen\Application;

use App\Screen\Application\DTO\ScreenListDTO;
use App\Screen\Application\Query\GetScreenListQuery;
use App\Screen\Domain\ScreenRepository;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class GetScreenListHandler
{
    public function __construct(
        private ScreenRepository $screenRepository
    ) {
    }

    public function __invoke(GetScreenListQuery $query): ScreenListDTO
    {
        return ScreenListDTO::createFromScreens(
            $this->screenRepository->getScreens()
        );
    }
}
