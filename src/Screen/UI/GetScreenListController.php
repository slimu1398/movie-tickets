<?php

declare(strict_types=1);

namespace App\Screen\UI;

use App\Screen\Application\DTO\ScreenListDTO;
use App\Screen\Application\Query\GetScreenListQuery;
use App\Screen\UI\Response\ScreenListResponseDTO;
use App\Shared\ApiController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class GetScreenListController extends ApiController
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $messageBus
    ) {
    }

    #[Route(name: 'get_list', methods: ['GET'])]
    public function __invoke(): JsonResponse
    {
        /** @var ScreenListDTO $screenListDTO */
        $screenListDTO = $this->handle(new GetScreenListQuery());

        return $this->json(ScreenListResponseDTO::createFromScreenListDTO($screenListDTO));
    }
}
