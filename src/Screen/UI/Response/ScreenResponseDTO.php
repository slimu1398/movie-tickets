<?php

declare(strict_types=1);

namespace App\Screen\UI\Response;

use App\Screen\Application\DTO\ScreenDTO;
use App\Shared\IdEncoder;

class ScreenResponseDTO
{
    /**
     * @param array<int, int> $seats
     */
    public function __construct(
        public string $id,
        public string $name,
        public array $seats
    ) {
    }

    public static function createFromScreenDTO(ScreenDTO $screenDTO): self
    {
        return new self(
            IdEncoder::encode($screenDTO->id),
            $screenDTO->name,
            $screenDTO->seats
        );
    }
}
