<?php

declare(strict_types=1);

namespace App\Screen\UI\Response;

use App\Screen\Application\DTO\ScreenListDTO;

readonly class ScreenListResponseDTO
{
    /**
     * @param array<int, ScreenResponseDTO> $screens
     */
    public function __construct(
        public array $screens
    ) {
    }

    public static function createFromScreenListDTO(ScreenListDTO $screenListDTO): self
    {
        $screenResponseDTOs = [];
        foreach ($screenListDTO->screenDTOs as $screenDTO) {
            $screenResponseDTOs[] = ScreenResponseDTO::createFromScreenDTO($screenDTO);
        }

        return new self($screenResponseDTOs);
    }
}
