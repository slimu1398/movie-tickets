<?php

declare(strict_types=1);

namespace App\Movie\UI;

use App\Movie\Application\Command\CreateReservationCommand;
use App\Movie\UI\Request\CreateReservationRequestDTO;
use App\Shared\ApiController;
use App\Shared\Id;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class CreateReservationController extends ApiController
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $messageBus
    ) {
    }

    #[Route('/schedule/{scheduleId}/reservation', name: 'schedule_reservation', requirements: ['scheduleId' => '^[a-zA-Z0-9]+'], methods: ['POST'])]
    public function __invoke(
        string $scheduleId,
        #[MapRequestPayload] CreateReservationRequestDTO $request
    ): JsonResponse {
        try {
            $this->handle(
                new CreateReservationCommand(
                    Id::fromEncodedId($scheduleId),
                    $request->seats
                )
            );
        } catch (HandlerFailedException $e) {
            return $this->handlerException($e);
        }

        return new JsonResponse(null, Response::HTTP_CREATED);
    }
}
