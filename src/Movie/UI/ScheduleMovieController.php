<?php

declare(strict_types=1);

namespace App\Movie\UI;

use App\Movie\Application\Command\ScheduleMovieCommand;
use App\Movie\UI\Request\ScheduleMovieRequestDTO;
use App\Shared\ApiController;
use App\Shared\Id;
use Carbon\CarbonImmutable;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class ScheduleMovieController extends ApiController
{
    use HandleTrait;

    public function __construct(
        private MessageBusInterface $messageBus
    ) {
    }

    #[Route('/{movieId}/schedule', name: 'schedule', requirements: ['movieId' => '^[a-zA-Z0-9]+'], methods: ['POST'])]
    public function __invoke(
        string $movieId,
        #[MapRequestPayload] ScheduleMovieRequestDTO $request
    ): JsonResponse {
        try {
            $this->handle(
                new ScheduleMovieCommand(
                    Id::fromEncodedId($movieId),
                    Id::fromEncodedId($request->screenId),
                    new CarbonImmutable($request->movieStartDate)
                )
            );
        } catch (HandlerFailedException $e) {
            return $this->handlerException($e);
        }

        return new JsonResponse(null, Response::HTTP_CREATED);
    }
}
