<?php

declare(strict_types=1);

namespace App\Movie\UI\Request;

use Symfony\Component\Validator\Constraints as Assert;

readonly class CreateReservationRequestDTO
{
    /**
     * @param array<int, int> $seats
     */
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Count(min: 1)]
        #[Assert\All([
            new Assert\NotBlank(),
            new Assert\Type('integer')
        ])]
        public array $seats
    ) {
    }
}
