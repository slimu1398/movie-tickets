<?php

declare(strict_types=1);

namespace App\Movie\UI\Request;

use Symfony\Component\Validator\Constraints as Assert;

readonly class ScheduleMovieRequestDTO
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Regex('/^[a-zA-Z0-9]+/')]
        public string $screenId,
        #[Assert\NotBlank]
        #[Assert\DateTime]
        public string $movieStartDate
    ) {
    }
}
