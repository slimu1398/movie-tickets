<?php

declare(strict_types=1);

namespace App\Movie\Application\Exception;

use App\Shared\NamedException;

abstract class MovieNamedException extends NamedException
{
    protected const MOVIE_NOT_FOUND_NAME = 'movie.notFound';
    protected const MOVIE_NOT_FOUND_CODE = 1001;
    protected const MOVIE_SCHEDULE_DATE_OCCUPIED_NAME = 'movie.scheduleDateOccupied';
    protected const MOVIE_SCHEDULE_DATE_OCCUPIED_CODE = 1002;
    protected const MOVIE_SCHEDULE_NOT_FOUND_NAME = 'movie.scheduleNotFound';
    protected const MOVIE_SCHEDULE_NOT_FOUND_CODE = 1003;
    protected const MOVIE_RESERVATION_SEATS_OCCUPIED_FOUND_NAME = 'movie.seatsOccupied';
    protected const MOVIE_RESERVATION_SEATS_OCCUPIED_FOUND_CODE = 1004;
}
