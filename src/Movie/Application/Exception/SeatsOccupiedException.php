<?php

declare(strict_types=1);

namespace App\Movie\Application\Exception;

class SeatsOccupiedException extends MovieNamedException
{
    public static function create(): self
    {
        return new self('Seats occupied', self::MOVIE_RESERVATION_SEATS_OCCUPIED_FOUND_CODE);
    }

    public function getName(): string
    {
        return self::MOVIE_RESERVATION_SEATS_OCCUPIED_FOUND_NAME;
    }
}
