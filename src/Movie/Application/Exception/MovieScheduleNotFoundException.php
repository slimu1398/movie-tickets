<?php

declare(strict_types=1);

namespace App\Movie\Application\Exception;

class MovieScheduleNotFoundException extends MovieNamedException
{
    public static function create(): self
    {
        return new self('Movie schedule not found', self::MOVIE_SCHEDULE_NOT_FOUND_CODE);
    }

    public function getName(): string
    {
        return self::MOVIE_SCHEDULE_NOT_FOUND_NAME;
    }
}
