<?php

declare(strict_types=1);

namespace App\Movie\Application\Exception;

class MovieNotFoundException extends MovieNamedException
{
    public static function create(): self
    {
        return new self('Movie not found', self::MOVIE_NOT_FOUND_CODE);
    }

    public function getName(): string
    {
        return self::MOVIE_NOT_FOUND_NAME;
    }
}
