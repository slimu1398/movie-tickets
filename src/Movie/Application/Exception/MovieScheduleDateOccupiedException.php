<?php

declare(strict_types=1);

namespace App\Movie\Application\Exception;

class MovieScheduleDateOccupiedException extends MovieNamedException
{
    public static function create(): self
    {
        return new self('Movie schedule date occupied', self::MOVIE_SCHEDULE_DATE_OCCUPIED_CODE);
    }

    public function getName(): string
    {
        return self::MOVIE_SCHEDULE_DATE_OCCUPIED_NAME;
    }
}
