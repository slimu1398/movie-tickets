<?php

declare(strict_types=1);

namespace App\Movie\Application\Command;

use App\Shared\Id;
use Carbon\CarbonImmutable;

readonly class ScheduleMovieCommand
{
    public function __construct(
        public Id $movieId,
        public Id $screenId,
        public CarbonImmutable $movieStartDate
    ) {
    }
}
