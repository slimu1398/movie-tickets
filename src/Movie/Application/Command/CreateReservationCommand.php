<?php

declare(strict_types=1);

namespace App\Movie\Application\Command;

use App\Shared\Id;

readonly class CreateReservationCommand
{
    /**
     * @param array<int, int> $seats
     */
    public function __construct(
        public Id $movieScheduleId,
        public array $seats
    ) {
    }
}
