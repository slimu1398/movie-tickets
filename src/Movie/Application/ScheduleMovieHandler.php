<?php

declare(strict_types=1);

namespace App\Movie\Application;

use App\Movie\Application\Command\ScheduleMovieCommand;
use App\Movie\Application\Exception\MovieNotFoundException;
use App\Movie\Application\Exception\MovieScheduleDateOccupiedException;
use App\Movie\Domain\Exception\CannotScheduleMovieWithGivenDateException;
use App\Movie\Domain\Exception\MovieNotFoundException as MovieNotFoundDomainException;
use App\Movie\Domain\MovieId;
use App\Movie\Domain\MovieRepository;
use App\Movie\Domain\MovieScheduleAvailabilityService;
use App\Movie\Domain\MovieScheduleRepository;
use App\Movie\Domain\ScreenId;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class ScheduleMovieHandler
{
    public function __construct(
        private MovieRepository $movieRepository,
        private MovieScheduleRepository $movieScheduleRepository,
        private MovieScheduleAvailabilityService $movieScheduleAvailabilityService
    ) {
    }

    /**
     * @throws MovieNotFoundException
     * @throws MovieScheduleDateOccupiedException
     */
    public function __invoke(ScheduleMovieCommand $command): void
    {
        try {
            $movie = $this->movieRepository->getById(
                MovieId::fromId($command->movieId)
            );

            $movieSchedule = $movie->schedule(
                ScreenId::fromId($command->screenId),
                $command->movieStartDate,
                $this->movieScheduleAvailabilityService
            );

            $this->movieScheduleRepository->persist($movieSchedule);
        } catch (MovieNotFoundDomainException) {
            throw MovieNotFoundException::create();
        } catch (CannotScheduleMovieWithGivenDateException) {
            throw MovieScheduleDateOccupiedException::create();
        }
    }
}
