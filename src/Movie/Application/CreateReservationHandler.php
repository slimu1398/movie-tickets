<?php

declare(strict_types=1);

namespace App\Movie\Application;

use App\Movie\Application\Command\CreateReservationCommand;
use App\Movie\Application\Exception\MovieScheduleNotFoundException;
use App\Movie\Application\Exception\SeatsOccupiedException;
use App\Movie\Domain\Exception\MovieScheduleNotFoundException as MovieScheduleNotFoundDomainException;
use App\Movie\Domain\Exception\SeatsOccupiedException as SeatsOccupiedDomainException;
use App\Movie\Domain\MovieScheduleId;
use App\Movie\Domain\MovieScheduleRepository;
use App\Movie\Domain\PaymentService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class CreateReservationHandler
{
    public function __construct(
        private MovieScheduleRepository $movieScheduleRepository,
        private PaymentService $paymentService
    ) {
    }

    /**
     * @throws MovieScheduleNotFoundException
     * @throws SeatsOccupiedException
     */
    public function __invoke(CreateReservationCommand $command): void
    {
        try {
            $movieSchedule = $this->movieScheduleRepository->getById(
                MovieScheduleId::fromId($command->movieScheduleId)
            );

            $movieReservation = $movieSchedule->createReservation($command->seats);
            $movieReservation->createPayment($this->paymentService);

            $this->movieScheduleRepository->persist($movieSchedule);
        } catch (MovieScheduleNotFoundDomainException) {
            throw MovieScheduleNotFoundException::create();
        } catch (SeatsOccupiedDomainException) {
            throw SeatsOccupiedException::create();
        }
    }
}
