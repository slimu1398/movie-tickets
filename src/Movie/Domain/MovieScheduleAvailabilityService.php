<?php

declare(strict_types=1);

namespace App\Movie\Domain;

use App\Movie\Domain\Exception\CannotScheduleMovieWithGivenDateException;
use App\Movie\Domain\Exception\MovieScheduleNotFoundException;
use Carbon\CarbonImmutable;

class MovieScheduleAvailabilityService
{
    private const MOVIE_SCHEDULE_BREAK_TIME = 30;

    public function __construct(
        private readonly MovieScheduleRepository $movieScheduleRepository
    ) {
    }

    /**
     * @throws CannotScheduleMovieWithGivenDateException
     */
    public function checkMovieScheduleAvailability(
        ScreenId $screenId,
        CarbonImmutable $movieStartDate,
        CarbonImmutable $movieEndDate
    ): void {
        try {
            $this->movieScheduleRepository->getByQuery(
                new MovieScheduleRepositoryQuery(
                    $screenId,
                    $movieEndDate->addMinutes(self::MOVIE_SCHEDULE_BREAK_TIME),
                    $movieStartDate->subMinutes(self::MOVIE_SCHEDULE_BREAK_TIME)
                )
            );

            throw CannotScheduleMovieWithGivenDateException::create();
        } catch (MovieScheduleNotFoundException) {
        }
    }
}
