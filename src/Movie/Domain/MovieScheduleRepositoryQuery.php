<?php

declare(strict_types=1);

namespace App\Movie\Domain;

use Carbon\CarbonImmutable;

readonly class MovieScheduleRepositoryQuery
{
    public function __construct(
        public ?ScreenId $screenId,
        public ?CarbonImmutable $movieStartDateTo,
        public ?CarbonImmutable $movieEndDateFrom
    ) {
    }
}
