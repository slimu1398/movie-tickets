<?php

declare(strict_types=1);

namespace App\Movie\Domain;

use App\Movie\Domain\Exception\MovieNotFoundException;

interface MovieRepository
{
    /**
     * @throws MovieNotFoundException
     */
    public function getById(MovieId $movieId): Movie;
}
