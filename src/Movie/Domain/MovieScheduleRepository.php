<?php

declare(strict_types=1);

namespace App\Movie\Domain;

use App\Movie\Domain\Exception\MovieScheduleNotFoundException;

interface MovieScheduleRepository
{
    public function persist(MovieSchedule $movieSchedule): void;

    /**
     * @throws MovieScheduleNotFoundException
     */
    public function getById(MovieScheduleId $movieScheduleId): MovieSchedule;

    /**
     * @return array<int, MovieSchedule>
     * @throws MovieScheduleNotFoundException
     */
    public function getByQuery(MovieScheduleRepositoryQuery $query): array;
}
