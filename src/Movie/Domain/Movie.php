<?php

declare(strict_types=1);

namespace App\Movie\Domain;

use App\Movie\Domain\Exception\CannotScheduleMovieWithGivenDateException;
use Carbon\CarbonImmutable;

class Movie
{
    public ?int $id;

    public function __construct(
        MovieId $movieId,
        public readonly string $name,
        public readonly int $duration
    ) {
        $this->id = $movieId->getId();
    }

    /**
     * @throws CannotScheduleMovieWithGivenDateException
     */
    public function schedule(
        ScreenId $screenId,
        CarbonImmutable $movieStartDate,
        MovieScheduleAvailabilityService $movieScheduleAvailabilityService
    ): MovieSchedule {
        return MovieSchedule::create(
            $this,
            $screenId,
            $movieStartDate,
            $movieScheduleAvailabilityService
        );
    }
}
