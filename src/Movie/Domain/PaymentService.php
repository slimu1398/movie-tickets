<?php

declare(strict_types=1);

namespace App\Movie\Domain;

interface PaymentService
{
    public function createPayment(MovieReservation $movieReservation): PaymentId;
}
