<?php

declare(strict_types=1);

namespace App\Movie\Domain;

use App\Shared\Id;

readonly class MovieReservationId extends Id
{
}
