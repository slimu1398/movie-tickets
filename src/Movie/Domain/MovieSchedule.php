<?php

declare(strict_types=1);

namespace App\Movie\Domain;

use App\Movie\Domain\Exception\CannotScheduleMovieWithGivenDateException;
use App\Movie\Domain\Exception\SeatsOccupiedException;
use Carbon\CarbonImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class MovieSchedule
{
    private ?int $id;
    public readonly int $screenId;

    private function __construct(
        MovieScheduleId $movieScheduleId,
        ScreenId $screenId,
        public readonly Movie $movie,
        public readonly CarbonImmutable $movieStartDate,
        public readonly CarbonImmutable $movieEndDate,
        /** @var Collection<int, MovieReservation> $movieReservations */
        private Collection $movieReservations = new ArrayCollection(),
    ) {
        $this->id = $movieScheduleId->getId();
        $this->screenId = $screenId->getIntValue();
    }

    /**
     * @throws CannotScheduleMovieWithGivenDateException
     */
    public static function create(
        Movie $movie,
        ScreenId $screenId,
        CarbonImmutable $movieStartDate,
        MovieScheduleAvailabilityService $movieScheduleAvailabilityService
    ): self {
        $movieEndDate = $movieStartDate->addMinutes($movie->duration);
        $movieScheduleAvailabilityService->checkMovieScheduleAvailability(
            $screenId,
            $movieStartDate,
            $movieEndDate
        );

        return new self(
            MovieScheduleId::nullInstance(),
            $screenId,
            $movie,
            $movieStartDate,
            $movieEndDate
        );
    }

    public function getId(): MovieScheduleId
    {
        return new MovieScheduleId($this->id);
    }

    /**
     * @return array<int, MovieReservation>
     */
    public function getMovieReservations(): array
    {
        return $this->movieReservations->toArray();
    }

    /**
     * @param array<int, int> $seats
     * @throws SeatsOccupiedException
     */
    public function createReservation(array $seats): MovieReservation
    {
        $reservation = MovieReservation::create($this, $seats);

        $this->movieReservations->add($reservation);

        return $reservation;
    }

    /**
     * @param array<int, int> $seats
     */
    public function areSeatsOccupied(array $seats): bool
    {
        $occupiedSeats = array_map(
            fn (MovieReservation $reservation): array => $reservation->seats,
            $this->getMovieReservations()
        );

        $occupiedSeats = array_merge(...$occupiedSeats);

        return count(array_intersect($seats, $occupiedSeats)) > 0;
    }
}
