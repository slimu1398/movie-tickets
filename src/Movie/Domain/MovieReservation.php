<?php

declare(strict_types=1);

namespace App\Movie\Domain;

use App\Movie\Domain\Exception\SeatsOccupiedException;
use Carbon\CarbonImmutable;

class MovieReservation
{
    private const SEAT_RESERVATION_PRICE = 17.99;

    private ?int $id;
    private ?int $paymentId;

    /**
     * @param array<int, int> $seats
     */
    private function __construct(
        MovieReservationId $reservationId,
        PaymentId $paymentId,
        public readonly MovieSchedule $movieSchedule,
        public readonly array $seats,
        private ?CarbonImmutable $expirationDate
    ) {
        $this->id = $reservationId->getId();
        $this->paymentId = $paymentId->getId();
    }

    /**
     * @param array<int, int> $seats
     * @throws SeatsOccupiedException
     */
    public static function create(MovieSchedule $movieSchedule, array $seats): self
    {
        if ($movieSchedule->areSeatsOccupied($seats)) {
            throw SeatsOccupiedException::create();
        }

        return new self(
            MovieReservationId::nullInstance(),
            PaymentId::nullInstance(),
            $movieSchedule,
            $seats,
            $movieSchedule->movieStartDate->subMinutes(30)
        );
    }

    public function getId(): MovieReservationId
    {
        return new MovieReservationId($this->id);
    }

    public function getPaymentId(): PaymentId
    {
        return new PaymentId($this->paymentId);
    }

    public function getExpirationDate(): ?CarbonImmutable
    {
        return $this->expirationDate;
    }

    public function calculateReservationPrice(): float
    {
        return self::SEAT_RESERVATION_PRICE * count($this->seats);
    }

    public function createPayment(PaymentService $paymentService): void
    {
        $paymentId = $paymentService->createPayment($this);
        $this->paymentId = $paymentId->getIntValue();
    }

    public function complete(): void
    {
        $this->expirationDate = null;
    }
}
