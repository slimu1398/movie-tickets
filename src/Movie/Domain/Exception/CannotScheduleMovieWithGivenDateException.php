<?php

declare(strict_types=1);

namespace App\Movie\Domain\Exception;

use Exception;

class CannotScheduleMovieWithGivenDateException extends Exception
{
    public static function create(): self
    {
        return new self('Cannot schedule movie with given date');
    }
}
