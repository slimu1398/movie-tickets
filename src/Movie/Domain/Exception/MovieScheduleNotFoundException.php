<?php

declare(strict_types=1);

namespace App\Movie\Domain\Exception;

use Exception;

class MovieScheduleNotFoundException extends Exception
{
    public static function create(): self
    {
        return new self('Movie schedule not found');
    }
}
