<?php

declare(strict_types=1);

namespace App\Movie\Domain\Exception;

use Exception;

class SeatsOccupiedException extends Exception
{
    public static function create(): self
    {
        return new self('One of the seat is occupied');
    }
}
