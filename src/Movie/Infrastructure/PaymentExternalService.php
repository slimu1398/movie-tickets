<?php

declare(strict_types=1);

namespace App\Movie\Infrastructure;

use App\Movie\Domain\MovieReservation;
use App\Movie\Domain\PaymentId;
use App\Movie\Domain\PaymentService;
use App\Payment\Application\Command\CreatePaymentCommand;
use App\Payment\Application\CreatePaymentHandler;

readonly class PaymentExternalService implements PaymentService
{
    public function __construct(
        private CreatePaymentHandler $createPaymentHandler
    ) {
    }

    public function createPayment(MovieReservation $movieReservation): PaymentId
    {
        $paymentId = $this->createPaymentHandler->__invoke(
            new CreatePaymentCommand($movieReservation->calculateReservationPrice())
        );

        return PaymentId::fromId($paymentId);
    }
}
