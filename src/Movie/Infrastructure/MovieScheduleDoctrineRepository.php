<?php

declare(strict_types=1);

namespace App\Movie\Infrastructure;

use App\Movie\Domain\Exception\MovieScheduleNotFoundException;
use App\Movie\Domain\MovieSchedule;
use App\Movie\Domain\MovieScheduleId;
use App\Movie\Domain\MovieScheduleRepository;
use App\Movie\Domain\MovieScheduleRepositoryQuery;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MovieSchedule>
 */
class MovieScheduleDoctrineRepository extends ServiceEntityRepository implements MovieScheduleRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MovieSchedule::class);
    }

    public function persist(MovieSchedule $movieSchedule): void
    {
        $this->getEntityManager()->persist($movieSchedule);
        $this->getEntityManager()->flush();
    }

    public function getById(MovieScheduleId $movieScheduleId): MovieSchedule
    {
        $movieSchedule = $this->find($movieScheduleId->getId());

        if (null === $movieSchedule) {
            throw MovieScheduleNotFoundException::create();
        }

        return $movieSchedule;
    }

    public function getByQuery(MovieScheduleRepositoryQuery $query): array
    {
        $qb = $this->createQueryBuilder('ms')
            ->select('ms');

        if (null !== $query->screenId) {
            $qb->andWhere($qb->expr()->eq('ms.screenId', ':screenId'))
                ->setParameter('screenId', $query->screenId->getIntValue());
        }

        if (null !== $query->movieStartDateTo) {
            $qb->andWhere($qb->expr()->lt('ms.movieStartDate', ':movieStartDateTo'))
                ->setParameter('movieStartDateTo', $query->movieStartDateTo);
        }

        if (null !== $query->movieEndDateFrom) {
            $qb->andWhere($qb->expr()->gt('ms.movieEndDate', ':movieEndDateFrom'))
                ->setParameter('movieEndDateFrom', $query->movieEndDateFrom);
        }

        $result = $qb->getQuery()->execute();

        if (empty($result)) {
            throw MovieScheduleNotFoundException::create();
        }

        return $result;
    }
}
