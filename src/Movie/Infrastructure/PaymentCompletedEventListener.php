<?php

declare(strict_types=1);

namespace App\Movie\Infrastructure;

use App\Movie\Domain\PaymentId;
use App\Payment\Application\Event\PaymentCompletedEvent;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[AsEventListener]
readonly class PaymentCompletedEventListener
{
    public function __construct(
        private MovieReservationDoctrineRepository $movieReservationRepository
    ) {
    }

    public function __invoke(PaymentCompletedEvent $event): void
    {
        $movieReservation = $this->movieReservationRepository->findByPaymentId(
            PaymentId::fromId($event->paymentId)
        );

        if (null === $movieReservation) {
            return;
        }

        $movieReservation->complete();

        $this->movieReservationRepository->persist($movieReservation);
    }
}
