<?php

declare(strict_types=1);

namespace App\Movie\Infrastructure;

use App\Movie\Domain\MovieReservation;
use App\Movie\Domain\PaymentId;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<MovieReservation>
 */
class MovieReservationDoctrineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MovieReservation::class);
    }

    public function persist(MovieReservation $movieReservation): void
    {
        $this->getEntityManager()->persist($movieReservation);
        $this->getEntityManager()->flush();
    }

    public function findByPaymentId(PaymentId $paymentId): ?MovieReservation
    {
        return $this->findOneBy(['paymentId' => $paymentId->getId()]);
    }
}
