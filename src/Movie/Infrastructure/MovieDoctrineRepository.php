<?php

declare(strict_types=1);

namespace App\Movie\Infrastructure;

use App\Movie\Domain\Exception\MovieNotFoundException;
use App\Movie\Domain\Movie;
use App\Movie\Domain\MovieId;
use App\Movie\Domain\MovieRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Movie>
 */
class MovieDoctrineRepository extends ServiceEntityRepository implements MovieRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Movie::class);
    }

    public function getById(MovieId $movieId): Movie
    {
        $movie = $this->find($movieId->getId());

        if (null === $movie) {
            throw MovieNotFoundException::create();
        }

        return $movie;
    }
}
