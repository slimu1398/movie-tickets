<?php

declare(strict_types=1);

namespace App\Ping\UI;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class PingController extends AbstractController
{
    #[Route(name: 'check', methods: ['GET'])]
    public function ping(): JsonResponse
    {
        return new JsonResponse('pong');
    }
}
