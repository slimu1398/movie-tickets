<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230626181141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE movie (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, duration INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movie_schedule (id INT AUTO_INCREMENT NOT NULL, movie_id INT NOT NULL, screen_id INT NOT NULL, movie_start_date DATETIME(6) NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', movie_end_date DATETIME(6) NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_86CE8A768F93B6FC (movie_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie_schedule ADD CONSTRAINT FK_86CE8A768F93B6FC FOREIGN KEY (movie_id) REFERENCES movie (id)');
        $this->addSql('INSERT INTO movie (name, duration) VALUES (\'First movie\', 97), (\'Second movie\', 120), (\'Third movie\', 135)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movie_schedule DROP FOREIGN KEY FK_86CE8A768F93B6FC');
        $this->addSql('DROP TABLE movie');
        $this->addSql('DROP TABLE movie_schedule');
    }
}
