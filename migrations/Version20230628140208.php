<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230628140208 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE movie_reservation (id INT AUTO_INCREMENT NOT NULL, movie_schedule_id INT NOT NULL, seats JSON NOT NULL, expiration_date DATETIME(6) NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', status VARCHAR(255) NOT NULL, INDEX IDX_6D4EC5A28B8D4E22 (movie_schedule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movie_reservation ADD CONSTRAINT FK_6D4EC5A28B8D4E22 FOREIGN KEY (movie_schedule_id) REFERENCES movie_schedule (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movie_reservation DROP FOREIGN KEY FK_6D4EC5A28B8D4E22');
        $this->addSql('DROP TABLE movie_reservation');
    }
}
